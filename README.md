

#Lis4381

##Justin Peterson

### Assignment #1 Requirements:

*Setting up SSH for Git:*

1. Ordered-list items
2. Generates an RSA key for use in SSH protocol
3. The result of this public key is stored in a file that holds the same name but ''.pub'' appended. The program also asks for a passphrase

#### README.md file should include the following items:

* Bullet-list items
* Generates an RSA key for use in SSH protocol
* The result of this public key is stored in a file that holds the same name but ''.pub'' appended. The program also asks for a passphrase
* 

> 
>
> #### Git commands w/short descriptions:

1. git init - creates an empty git repository
2. git status  - shows status of our working tree
3. git add - add one or more files to an index
4. git commit - record changes to the repository
5. git push - sends the changes to the master branch of the remote repository
6. git pull - merger the changes from the remote repository to the working directory
7. git clone - clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:


*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/juddinp9/web-app-development "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/juddinp9/myteamqoutes "My Team Quotes Tutorial")